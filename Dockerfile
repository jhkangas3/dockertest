FROM ubuntu

WORKDIR /app

RUN ls -al
RUN apt-get update --yes
RUN apt install --yes fortune

ENTRYPOINT bash ./runfortune.sh