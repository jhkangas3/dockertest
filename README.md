## A fortune docker container

This Docker container when ran prints out a fortune using a fortune app.

Container runs on Ubuntu.

## Usage 
To pull image
`docker pull registry.gitlab.com/jhkangas3/dockertest:latest`
To run
`docker run --rm -v $(pwd):/usr/src/ -w /usr/src/ gcc/build `


## Docker usecase
MySql server
https://hub.docker.com/_/mysql

A mysql server can be set up usign a docker container.



# Screenshot of using cowsay inside a  container
![](./dockertest.JPG)